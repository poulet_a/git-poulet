#git-poulet


version : [![GitHub version](https://badge.fury.io/gh/pouleta%2Fgit-poulet.svg)](http://badge.fury.io/gh/pouleta%2Fgit-poulet)  
version : 1.0.alpha5  


## Instructions
put it in your `$PATH` (/usr/bin for example)  
``git poulet -h``  


## Use
This is a simple git extention to improve your tagging experience.  
The tagging create a tag like **vX.Y-stateZ**  


## Options
-d tag		: delete a tag version
-d -n tag	: delete tag (does not interpret the tag as version)
tag		: tagging tag version
-n tag		: tagging (does not interpret the tag as version)


## TODO
### [Feature] Mots clefs
Quand l'un des mots clef est trouvé, passer le state à l'état.  
Si cet état était déjà la, incrémenter la version **Z+1 **

**Exemples**
- **v1.0-dev2** ; git poulet alpha => **v1.0-alpha1**
- **v1.0-aplha1** ; git poulet alpha => **v1.0-alpha2**

**Liste des mots clefs**
- a, **alpha**
- b, **beta**
- **rc**

### [Fix] Amélioration des options de base
- option **-d** (vérifier et fixer)
- option **-a** pour empecher la signature (todo)
- option **-s** pour forcer la signature (todo)
