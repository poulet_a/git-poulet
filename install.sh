#!/bin/sh
## install.sh for  in /home/poulet_a/projets/git-poulet
## 
## Made by poulet_a
## Login   <poulet_a@epitech.eu>
## 
## Started on  Thu Sep  4 18:52:38 2014 poulet_a
## Last update Fri Sep  5 16:46:22 2014 poulet_a
##

[[ "$UID" == "0" ]] || (echo "You cannot perform that unless you are sudo. Check the script if you do not trust me :(" && exit 1)

if [[ "$1" == "" || "$1" == "install" ]]
then
    cp -v git-poulet /usr/bin &> /dev/null
    [[ "$?" == "0" ]] && echo "git-poulet installed successfully" || (echo "Installation failed. You should cpy git-poulet manualy as sudo" && exit 1)
    file "/usr/bin/git-poulet" &> /dev/null
    [[ "$?" == "0" ]] && echo "git-poulet is now located in /usr/bin/git-poulet"
    [[ "$PATH | grep '/usr/bin'" == "" ]] && echo "PATH in not well installed. Please add /usr/bin to your \$PATH environnement"
    exit 0

elif [[ "$1" == "uninstall" ]]
then
    rm -vf "/usr/bin/git-poulet" && echo "Uninstallation sucessfully" && exit 0 || (echo "Failed to uninstall git-poulet. Remove /usr/bin/git-poulet as sudo manualy" && exit 2)

else
    echo "Use install or uninstall"
    exit 0
fi
